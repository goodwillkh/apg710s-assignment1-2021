## Creators1: Goodwill T. Khoa  219080534 
## Creators2: Denzel J. Khama 217121268

# APG710s Assignment1 2021

Magic Game Created by Nust Studendts 2021

Game Instructions and Steps

To play this game you will need to
1.	Provide  a Name
2.	Think of a card or number
3.	Answer 4 Questions Only

Instructions:
Multi-Player Applications
1.	Enter Number of Players
2.	Enter Names of each player when prompted
3.	Each player think of a number between 1 and 52 (cards)
4.	Each player gets a turn to answer the four questions
5.	A player may only answer as prescribed by the AI
